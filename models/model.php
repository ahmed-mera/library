<?php
//use the namespace
namespace model;
//class model to save and to send the data
class model {
    public $next_id ;
       //to read the client data
    public function readDataClient () {
        $fp = fopen(__DIR__.'/data/data.csv','r');
        while (1) {
            if (feof($fp) ) : 
                break;
            else: 
                $dataOfClients[] = fgetcsv($fp);
            endif;
        }
        fclose($fp);
        return $dataOfClients;
    }

    //to get all books with them descriptions from file
    public function getBooks()
    {
        $fp = fopen(__DIR__.'/data/all_books.csv','r');
        $i = 0;
        while (!feof($fp)) {
                $book[] = fgetcsv($fp);
                $i++;
        }
        //for next element id 
        $this->next_id = $i;
        for ($i=0; $i < count($book) -1; $i++):
                $nameBook = $book[$i][0];
            // foreach ($book[$i] as $descriptions):
                $books[$nameBook]['name_book']           = $book[$i][0];//name book 
                $books[$nameBook]['src_img']             = $book[$i][1];//src image [ cover book]
                $books[$nameBook]['lang']                = $book[$i][2];//languages
                $books[$nameBook]['writer']              = $book[$i][3];//writer
                $books[$nameBook]['description']         = $book[$i][4];//description
                $books[$nameBook]['available']           = $book[$i][5];// available
                $books[$nameBook]['id']                  = $book[$i][6];//book id
                $books[$nameBook]['date']                = $book[$i][7];//date normal
                $books[$nameBook]['expire_date']         = $book[$i][8];//expire date 
            // endforeach;
        endfor;
        return $books;
    }

    //redirect 
    public function redirect ($msg='',$time = 3,$where = 'index.php?req=all_books'){
        echo $msg;
        header('refresh:'.$time.';url='.$where);
    } 


    //login
    public function login ($username,$password,&$status){
            $allData = model::readDataClient();//all data 
            for ($i=0; $i < count($allData) ; $i++):
                if($username == $allData[$i][1] && password_verify($password,$allData[$i][2])):
                    $status = array('fullname' => $allData[$i][0] );//his name 
                    $status['username'] = $allData[$i][1];//his email 
                    $status['password'] = $allData[$i][2];//his password
                    $status['user_id']  = $allData[$i][3];//his id
                    $status['privileges'] = $allData[$i][4];//his privileges
                    
                    if($status['privileges'] == 1){
                        //session for admin 
                        $_SESSION['user_name'] = $status['fullname'];
                        $_SESSION['email']     = $status['username'];
                        $_SESSION['password']  = $status['password'];
                        $_SESSION['user_id']   = $status['user_id'];
                        $_SESSION['privileges']  = $status['privileges'];
                    }else{
                         //session for user 
                        $_SESSION['user_name'] = $status['fullname'];
                        $_SESSION['email']     = $status['username'];
                        $_SESSION['password']  = $status['password'];
                        $_SESSION['user_id']   = $status['user_id'];
                        $_SESSION['privileges'] = $status['privileges'];
                    }

                    return true;
                else:
                    $status = '<p class=lead> Username or Password doesn\'t exist </p>';//message error
                endif;
            endfor;
        return false;
    }

    //sing in 
    public function register ($data_user,&$status) {
        $allData = model::readDataClient();//all data 
        //check if user exists or not
        for ($i=0; $i < count($allData) ; $i++) { 
                if ($data_user[0] == $allData[$i][0] ) {
                    $status [] = '<strong> * Name   </strong> is already exist <br />';
                }

                if ($data_user[1] == $allData[$i][1] ) {
                    $status [] = '<strong> * Email   </strong> is already exist <br />';
                }
        }
        
        if(preg_match('/^[$\/&-)\\=%!+*(|(\[\])]/', $data_user[0] ) ):
            $status [] =  '<p class=lead>  <strong> *  Field Full Name  </strong> Must not contain special characters, or emoji. </p> '; 
        endif;
        if(preg_match('/^[$\/&-)\\=%!+*(|(\[\])]/', $data_user[1] ) ):
            $status [] =  '<p class=lead> <strong> *  Field User Name ( E-mail )  </strong> Must not contain special characters, or emoji.</p> '; 
        endif;
        if(preg_match('/^[$\/&-)\\=%!+*(|(\[\])]/', $data_user[2] ) ):
            $status [] =  '<p class=lead>  <strong> *  Field Password  </strong> Must not contain special characters, or emoji. </p> '; 
        endif;

                //check password
            if ( preg_match('/^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]{6,10}$/', $data_user[2] )) {
                if ($data_user[2] !== $data_user[3]) { 
                    $status [] = ' <p class=lead> <strong>* Password and confirm Password</strong> Must be equal </p> '; 
                    return false;
                }
            }else{
                $status [] = '<p class=lead>  <strong> *  Your password </strong> Must be 6-10 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji. </p> '; 
                return false;
            }
            //if all right 
            if (empty($status )){
                //create the directory
                $dirUser = $data_user[1];
                $dirClient = __DIR__.'/data/users/'.$dirUser;//directory of user
                if(! is_dir($dirClient)){
                    mkdir($dirClient);//create directory
                    chmod($dirClient,0777);//change privileges
                    touch($dirClient.'/books.csv');//create file csv
                    chmod($dirClient.'/books.csv',0777);//change privileges

                }
                $data_user [] = random_int(0,1000); //casual id 0 - 10000
                $data_user [] = 0; //id normal client [ 0 ] admin will be [ 1 ]
                $data_user [2] = password_hash($data_user [2],PASSWORD_BCRYPT) ;//hasing the password
                unset($data_user[3]);//delete confirm password
                $fp = fopen(__DIR__.'/data/data.csv','a');
                    fputcsv($fp , $data_user); //save data
                fclose($fp);
                return true ;
            }
        }

    public function forgotPassword ($data_user , &$status) {
        $allData = model::readDataClient();//all data 

        if (empty($status)){
            if ( preg_match('/^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]{6,10}$/', $data_user[2] )) {
                if ($data_user[2] !== $data_user[3]) { 
                    $status [] = ' <p class=lead> <strong>* Password and confirm Password</strong> Must be equal </p> '; 
                }
            }else{
                $status [] = '<p class=lead>  <strong> *  Your password </strong> Must be 6-10 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji. </p> '; 
            }
        }
        //if all right 
        if (empty($status)){
            $users = 0 ;
            //check if user exists or not
            for ($i = 0; $i < count($allData); $i++) { 
                if ($data_user[0] == $allData[$i][0] && $data_user[1] == $allData[$i][1] ) { 
                    $allData[$i][2] = password_hash($data_user[2],PASSWORD_BCRYPT);//hasing the password and rest password
                }else{ $users++; }
            }
            if($users != count($allData)){
                $fp = fopen(__DIR__.'/data/data.csv','w');
                for ($i=0; $i < count($allData); $i++) {
                    @fputcsv($fp,$allData[$i]); //save data
                }
                fclose($fp);
                return true ;
            }else{
                $status [] = ' <p class=lead> <strong>* Name or Email</strong> doesn\'t exist </p> '; 
                return false;
            }
        }
        $status [] = ' <p class=lead> <strong>* Name or Email</strong> doesn\'t exist </p> '; 
        return false;
    }


    public function prenota ($books){
        $fp = fopen(__DIR__.'/data/users/'.$_SESSION['email'].'/books.csv','a');//open file 
        fputcsv($fp,$books);//save data
        fclose($fp);
    }

    public function readFileClient (){
        $fp = fopen(__DIR__.'/data/users/'.$_SESSION['email'].'/books.csv','r');//open file 
        while (! feof($fp)) { $books[] = fgetcsv($fp);/* read data */   }
        fclose($fp);
        for ($i=0; $i < count($books) -1; $i++):
            $nameBook = $books[$i][0];
            $books_client[$nameBook]['name_book']           = $books[$i][0];//name book 
            $books_client[$nameBook]['src_img']             = $books[$i][1];//src image [ cover book]
            $books_client[$nameBook]['date']                = $books[$i][2];//date normal
            $books_client[$nameBook]['expire_date']         = $books[$i][3];//expire date 
        endfor;
        return  @$books_client  ;
    }

    public function deleteBook ($bookName){
        $book = model::readFileClient();
        unset($book[$bookName]);//delete this book 
        $fp = fopen(__DIR__.'/data/users/'.$_SESSION['email'].'/books.csv','w');//open file 
        foreach ($book as $key => $value): 
            fputcsv($fp,$book[$key]);//save data
        endforeach;
        fclose($fp);
    }
}
