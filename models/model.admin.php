<?php
//use the namespace
namespace model\admin;

//class model to save and to send the data
class model {
    public $next_id;
           //to read the client data
        public function readDataClient () {
            $fp = fopen(__DIR__.'/data/data.csv','r');
            while (1) {
                if (feof($fp) ) : 
                    break;
                else: 
                    $dataOfClients[] = fgetcsv($fp);
                endif;
            }
            fclose($fp);
            return $dataOfClients;
        }
        //to get all books with them descriptions from file
        public function getBooks()
        {
            $fp = fopen(__DIR__.'/data/all_books.csv','r');
            $i = 0;
            while (!feof($fp)) {
                    $book[] = fgetcsv($fp);
                    $i++;
            }
            //for next element id 
            $this->next_id = $i;
            for ($i=0; $i < count($book) -1; $i++):
                    $nameBook = $book[$i][0];
                // foreach ($book[$i] as $descriptions):
                    $books[$nameBook]['name_book']           = $book[$i][0];//name book 
                    $books[$nameBook]['src_img']             = $book[$i][1];//src image [ cover book]
                    $books[$nameBook]['lang']                = $book[$i][2];//languages
                    $books[$nameBook]['writer']              = $book[$i][3];//writer
                    $books[$nameBook]['description']         = $book[$i][4];//description
                    $books[$nameBook]['available']           = $book[$i][5];// available
                    $books[$nameBook]['id']                  = $book[$i][6];//book id
                    $books[$nameBook]['date']                = $book[$i][7];//date normal
                    $books[$nameBook]['expire_date']         = $book[$i][8];//expire date 
                // endforeach;
            endfor;
            return $books;
        }
    // add new book
    public function addNewBook ($dataNewBook , $coverBook , &$err){
        $controlBookName = model::getBooks();
        //control if the book was insert or not
        foreach ($controlBookName as $key => $value):
        if(strtolower( $dataNewBook[0]) == strtolower( $controlBookName[$key]['name_book']))
        {$err [] ='<strong> * '. $dataNewBook[0]. '</strong> This book already exists. <br /> '; }
        endforeach;
        //controll all fields
        for ($i=0; $i < count($dataNewBook) ; $i++) { 
            //the pattern not allowed
            if( preg_match('/[$\/)\%+*((\[\])]+$/', $dataNewBook[$i] ) ):
                $err [] =  '<strong> * [ '. $dataNewBook[$i]. ' ]</strong> Must not contain special characters, or emoji. <br /> '; 
            endif;
        }
        
        //check all input by them length
        //name book
        if(  strlen($dataNewBook[0] ) < 3 ):
            $err  [] =  '<strong> * Filed Book Name </strong> Must enter a real Book Name . <br /> '; 
        endif;
        //name writer
        if(  strlen($dataNewBook[3] ) < 3 ):
            $err [] =  '<strong> * Filed  Writer </strong> Must enter a real Writer Name . <br /> '; 
        endif;
        //language
        if(  strlen($dataNewBook[2] ) < 2 ):
            $err [] =  '<strong> * Filed Language </strong> Must Select al Most One Language . <br /> '; 
        endif;
        //description
        if(  strlen($dataNewBook[4] ) < 10 && !empty($dataNewBook[4])):
             $err [] =  '<strong> * Filed  Description </strong> Must enter a real description for this book. <br /> '; 
        endif;

        //check if not exists the cover book
        if($coverBook['error'] == 4){ $coverBook = './views/images/no-book-cover.png'; $dataNewBook[1] = $coverBook;}
        else{
            //extention allowed
            $extention = ['jpg' , 'png', 'jpeg' , 'gif'];
            //get extention file
            $coverBookExtention = explode('.',$coverBook['name']);
            // dir all images
            $dirImages = strtolower('./views/images/'. basename($coverBook['name']));
            //check if the file exists
            if (file_exists(   $dirImages ) ) {
                $err [] = '<strong> * '. $coverBook['name'].'</strong>  Image already exists <br />'; 
            } 
            //check the size of file
            if ($coverBook['size'] >  1000000 ) { //1000000 byte = 1000 kb = 1megaByte
                $err [] =  '<strong> *  Size Image </strong> Must be less than or equal 1000KB ( 1 megaByte ) <br />'; 
            }
            //check the extention of file if allowed or not
            $coverBookExtention = strtolower(end($coverBookExtention));
            if (!in_array($coverBookExtention,$extention)) {
                $err [] =  '<strong> * </strong> i can not upload this image for security reasons, try with another image <br />'.$coverBook[0]; 
            }
            //check if all right
            if (empty($err)) {
                move_uploaded_file($coverBook['tmp_name'], $dirImages );
                $dataNewBook[1] = $dirImages;
            }
        }
        //if all right, save all data 
        if (empty($err)) {
            //set date
            $dataNewBook [] = date("d/m/Y - H:i:s",time());//for user & admin
            $dataNewBook [] = strtotime(date('Y-m-d h:i:s',time()));//for calcution expire date
            $fp = fopen(__DIR__.'/data/all_books.csv','a');
            fputcsv($fp,$dataNewBook);
            fclose($fp);
            return true;
        } else { 
            return false ;
        }
        $this->next_id += 1; //increment +1
    }

    // add new book
    public function updateBook ($dataNewBook , $coverBook , &$err , $current_cover){
        $controlBookName = model::getBooks();
        $counterBook = 0;
        //control if the book was insert or not
        foreach ($controlBookName as $key => $value):
            // global $counterBook;
            if( strtolower( $dataNewBook[0]) == strtolower( $controlBookName[$key]['name_book']) ){$counterBook++;}
                //$err [0] ='<strong> * '. $dataNewBook[0]. '</strong> This book already exists. <br /> ';
        endforeach;
        if($counterBook > 1 ){ $err [] ='<strong> * '. $dataNewBook[0]. '</strong> This book already exists. <br /> '; }
        //controll all fields
        for ($i=0; $i < count($dataNewBook) ; $i++) { 
            //the pattern not allowed
            if( preg_match('/[$\/)\%+*((\[\])]+$/', $dataNewBook[$i] ) ):
                $err [] =  '<strong> * [ '. $dataNewBook[$i]. ' ]</strong> Must not contain special characters, or emoji. <br /> '; 
            endif;
        }
        
        //check all input by them length
        //name book
        // if(  strlen($dataNewBook[0] ) < 3 ):
        //     $err  [] =  '<strong> * Filed Book Name </strong> Must enter a real Book Name . <br /> '; 
        // endif;
        // //name writer
        // if(  strlen($dataNewBook[3] ) < 3 ):
        //     $err [] =  '<strong> * Filed  Writer </strong> Must enter a real Writer Name . <br /> '; 
        // endif;
        // //language
        // if(  strlen($dataNewBook[2] ) < 2 ):
        //     $err [] =  '<strong> * Filed Language </strong> Must Select al Most One Language . <br /> '; 
        // endif;
        // //description
        // if(  strlen($dataNewBook[4] ) < 10 && !empty($dataNewBook[4])):
        //      $err [] =  '<strong> * Filed  Description </strong> Must enter a real description for this book. <br /> '; 
        // endif;

        //check if not exists the cover book
        if($coverBook['error'] == 4){$dataNewBook[1] = $current_cover;}
        else{
            //extention allowed
            $extention = ['jpg' , 'png', 'jpeg' , 'gif'];
            //get extention file
            $coverBookExtention = explode('.',$coverBook['name']);
            // dir all images
            $dirImages = strtolower('./views/images/'. basename($coverBook['name']));
            //check if the file exists
            if (file_exists(   $dirImages ) ) {
                $err [] = '<strong> * '. $coverBook['name'].'</strong>  Image already exists <br />'; 
            } 
            //check the size of file
            if ($coverBook['size'] >  1000000 ) { //1000000 byte = 1000 kb = 1megaByte
                $err [] =  '<strong> *  Size Image </strong> Must be less than or equal 1000KB ( 1 megaByte ) <br />'; 
            }
            //check the extention of file if allowed or not
            $coverBookExtention = strtolower(end($coverBookExtention));
            if (!in_array($coverBookExtention,$extention)) {
                $err [] =  '<strong> * </strong> i can not upload this image for security reasons, try with another image <br />'.$coverBook[0]; 
            }
            //check if all right
            if (empty($err)) {
                move_uploaded_file($coverBook['tmp_name'], $dirImages );
                $dataNewBook[1] = $dirImages;
            }
        }
        //if all right, save all data 
        if (empty($err)) {
            // //set date
            // $dataNewBook [] = date("d/m/Y - H:i:s",time());//for user & admin
            // $dataNewBook [] = strtotime(date('Y-m-d h:i:s',time()));//for calcution expire date
                //control if the book was insert or not
            // global $counterBook;
            //check name book
            // if($counterBook == 1){
                foreach ($controlBookName as $key => $value):
                    if(strtolower( $dataNewBook[6]) == strtolower( $controlBookName[$key]['id'])){
                        $controlBookName[$key]['name_book']           = $dataNewBook[0];//name book 
                        $controlBookName[$key]['src_img']             = $dataNewBook[1];//src image [cover book]
                        $controlBookName[$key]['lang']                = $dataNewBook[2];//languages
                        $controlBookName[$key]['writer']              = $dataNewBook[3];//writer
                        $controlBookName[$key]['description']         = $dataNewBook[4];//description
                        $controlBookName[$key]['available']           = $dataNewBook[5];// available
                        $controlBookName[$key]['id']                  = $dataNewBook[6];//book id
                        // $controlBookName[$key]['date']                = $dataNewBook[7];//date normal
                        // $controlBookName[$key]['expire_date']         = $dataNewBook[8];//expire date 
                    }   
                endforeach;                 
            // }            
            $fp = fopen(__DIR__.'/data/all_books.csv','w');
            foreach ($controlBookName as $key => $value) {
                fputcsv($fp,$controlBookName[$key]);
            }
            fclose($fp);
            return true;

        } else { 

            return false ;
        }
    }


     // add new user
    public function addNewUser ($dataNewUser , &$err){
        $dataOfClients = model::readDataClient();
        //control if the book was insert or not
        for ($i = 0;$i < count($dataOfClients) ; $i++):
        if($dataNewUser[0] == $dataOfClients[$i][0] )
            {$err [] ='<strong> * '. $dataNewUser[0]. '</strong> This user already exists. <br /> '; }
        if( $dataNewUser[1] == $dataOfClients[$i][1] )
            {$err [] ='<strong> * '. $dataNewUser[1]. '</strong> This username already exists. <br /> '; }
        endfor;
        //controll all fields
        for ($i = 0; $i < 2 ; $i++) { //check email & user name
            //the pattern not allowed
            if( preg_match('/[$\/&-)\\=%!+*(|(\[\])]/', $dataNewUser[$i] ) ):
                $err [] =  '<strong> * [ '. $dataNewUser[$i]. ' ]</strong> Must not contain special characters, or emoji. <br /> '; 
            endif;
        }
        
        //check password
        // if ( preg_match('/^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]{6,10}$/', $dataNewUser[2] )) {
            if ($dataNewUser[2] != $dataNewUser[3]) { 
                $err [] = ' <p class=lead> <strong>* Password and confirm Password</strong> Must be equal </p> '; 
            }
        // }else{
        //     $err [] = '<p class=lead>  <strong> *  Your password </strong> Must be 6-10 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji. </p> '; 
        // }      
         //name user
        if(strlen($dataNewUser[0] ) < 3 ):
            $err  [] =  '<p class=lead> <strong> * Fake Name </strong> Must enter a real  Name </p>'; 
        endif;
        //email
        if(strlen($dataNewUser[1] ) < 6 ):
            $err [] =  '<p class=lead> <strong> * Fake  Email </strong> Must enter a real Email . </p> '; 
        endif;

        //if all right, save all data 
        if (empty($err)) {
            $dataNewUser[2] = password_hash($dataNewUser [2],PASSWORD_BCRYPT) ;//hasing the password
            unset($dataNewUser[3]);//delete confirm password
            $dirClient = __DIR__.'/data/users/'.$dataNewUser[1];//directory of user
            if(! is_dir($dirClient)){
                mkdir($dirClient);//create directory
                chmod($dirClient,0777);//change privileges
                touch($dirClient.'/books.csv');//create file csv
                chmod($dirClient.'/books.csv',0777);//change privileges
            }
            $fp = fopen(__DIR__.'/data/data.csv','a');
            fputcsv($fp,$dataNewUser);
            fclose($fp);
            return true;
        } else { 
            return false ;
        }
    }
    public function deleteBook ($bookName){
        $book = model::getBooks();
        unset($book[$bookName]);//delete this book 
        $fp = fopen(__DIR__.'/data/all_books.csv','w');//open file 
        foreach ($book as $key => $value): 
            fputcsv($fp,$book[$key]);//save data
        endforeach;
        fclose($fp);
    }
    public function deleteUser ($userId){
        $users = model::readDataClient();
        //unlink(__DIR__.'/data/users/'.$users[$userId][1].'books.csv');//remove file
        unset($users[$userId]);//delete this book
        $fp = fopen(__DIR__.'/data/data.csv','w');//open file 
        for ($i = 0; $i <  count($users); $i++): 
            @fputcsv($fp,$users[$i]);//save data
        endfor;
        fclose($fp);
    }
    public function updateUser ($data){
        $fp = fopen(__DIR__.'/data/data.csv','w');//open file 
        for ($i = 0; $i <  count($data); $i++): 
            @fputcsv($fp,$data[$i]);//save data
        endfor;
        fclose($fp);
    }
    //redirect 
    public function redirect ($msg='',$time = 3,$where = 'index.php?req=dashboard'){
        echo $msg;
        header('refresh:'.$time.';url='.$where);
    } 

}
