





// page add a new book
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

//information password
$('.pass-sign-up , .pass-forgot').on('focus blur',function () {
    $(this).siblings('.information').toggle(300);
    // $(this).siblings('.form-sign-up > .form-group > i').toggleClass('focus');
});

$('.show-pass').click(function () {
    if($('.pass-login').attr('type') == 'text'){ $('.pass-login').attr('type','password'); }
    else{ $('.pass-login').attr('type','text'); }
    $(this).toggleClass('fa-eye fa-eye-slash');

});
var heightNav = $('.navbar').innerHeight() + 20;
$('.carousel').css('top', $('.navbar').height() - heightNav + 17 );
$('body').css('paddingTop',heightNav+'px');
$('img').each(function () {
var datePhp = (  $(this).attr('data-expire')/ (60*60*24) +5 ),//date in seconds + 5giorni
    dateJs = Date.parse(new Date ()) / (60*60*24*1000); //date in mille seconds
    dateExpire = Math.round(datePhp - dateJs );
    if(dateExpire > 0 ){
        $(this).siblings('.new').delay(200).show(500);
    }else{
        $(this).siblings('.new').hide(600);
    }
});
/* NOTE: calculate date by js 
    Math.round(Date.parse(new Date())/(60*60*24*1000) - (1559006823+(60*60*24)*7)/(60*60*24))
*/


//carousel
$('.carousel').carousel({
    ride: true,
    pause:false,
});
//search
var inputSearch = $('.input-search'),
    risSearch = $('.ris-search');
$('.search').on('click',function(){
    inputSearch.slideToggle(500).focus();
});

inputSearch.css('top', (heightNav - 20 ) +'px');
risSearch.css('top', ( heightNav + ( inputSearch.innerHeight() - 18) ) +'px');

$(window).click(function(){
    risSearch.slideUp(300);

});

$('.navbar-toggler').on('click',function(){ $(this).children('i').toggleClass('rotate')} );

$('.icon-search').click(function(){
    if ($(window).width() <= 1000) {
        $('.navbar-toggler').click();
    }
});
var books = document.getElementsByClassName('content-text');//.childNodes[0];
inputSearch.keyup(function () {
    var search = inputSearch.val().toLowerCase();
    if(search != '' ){
        var nameBook = '';
        for(i = 0; i < books.length ; i++){
            let name = books[i].innerText.toLowerCase();
            if(name.indexOf(search) > -1){
                nameBook += '<p class = "lead"><a href ="index.php?req='+ name + '" >' + books[i].innerText + '</a></p>'  ;
                risSearch.slideDown(300).html(nameBook );
            }
        }
    }else{
        risSearch.slideUp(300);
    }
});


//delete for user
$('.delete').click(function (e) {
    e.preventDefault();
    var book = $(this);
    if (confirm('Are you sure to delete it ?')) {
        const nameBook = book.data('name_book');
        $.post("?req=del-book",{ name : nameBook },
        function (data) {
            book.parents('.informationBook').toggle(500).prev().toggle(500);
            const elements =  $('.num-element');
            const numberElements =  parseInt( elements.text() ) - 1;
            elements.text(numberElements);
            // if($('col-12').hasClass('.informationBook')){
            //     $('.informationBook , .title').toggle();
            //     $('.alterText').toggle(400);
            // }else{
            //     $('.informationBook , .title').toggle(); 
            //     $('.alterText').toggle(400);
            // }
        });
    }
});

//delete for admin
$('.delete-admin').click(function (e) {
    e.preventDefault();
    var book = $(this);
    if (confirm('Are you sure to delete it ?')) {
        const nameBook = book.data('book');
        $.post("?req=del-book",{ name : nameBook },
        function (data) {
            // console.log(data);
            book.parents('.settings_book').toggle(500);
        });
    }
});

//delete for admin users
$('.delete-user-link').click(function (e) {
    e.preventDefault();
    var user = $(this);
    if (confirm('Are you sure to delete this user ?')) {
        const userID = user.data('id');
        $.post("?req=del-user",{ userId : userID },
        function (data) {
            console.log(data);
            user.parents('.data-user').toggle(500).prev().toggle(500);
        });
    }
});
// //edit
// $('.edit').click(function () {
//     $(this).parents('tr').children().children('input').removeAttr('readonly');
//     $(this).toggle(500).parent().find('.done').toggle(500);
// });

// //save
// $('.done').click(function () {
//     $(this).parents('tr').children().children('input').attr('readonly','');
//     $(this).toggle(500).parent().find('.edit').toggle(500);
// });
/*

$('.update_book').click(function(e){
    e.preventDefault();
    var inputs = $('.form-update-book input');
    for (let index = 0; index < inputs.length - 2; index++) {
        if(inputs[index].value == ''){
            const a = inputs[index];
            a.classList.add('is-invalid');
        }
    }
    console.log(inputs[8].files);
});*/

$('.NO-BOOK').click(function(e){
    e.preventDefault();
    $('.pop').toggle(500);
});

$('.pop .btn').click(function () {
    $(this).parents('.pop').toggle(500);
});
if($(window).width() <= 1000){ $($('.line')[0]).addClass('d-none') } else{ $($('.line')[0]).removeClass('d-none') }
//controllers\controller.php
/*$('.BOOK').click(function(e){
    e.preventDefault();
    const nameBook = $(this).data('name-book');
    $.post("?req=prenota", {name : nameBook},
        function (data) {
            const number = parseInt(data);
             data = data.trim(); 
             console.log(data);
             console.log(data.indexOf('false'));
                if(data == 'false'){
                    alert('no oh');
                // if(data.indexOf('false') != 0){console.log(data.indexOf('fara-false'));}else{console.log('no');}
                 }else{
                    alert('no else');

                 }
            //$('.pop-booked > p').parent().toggle(200).text(data);
            
        });

});*/
