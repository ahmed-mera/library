<?php
//check if isset session admin or not
if(!isset($_SESSION['privileges']) && $_SESSION['privileges'] != 1){
    $this->model_admin->redirect('',1,'index.php?req=login');
    exit;
}
?>
<style> body{ background: #EEE} </style>
<div class="container pt-3">
    <h1 class="text-center mb-5 statistics mt-3">statistics</h1>
    <div class="row text-center mt-3">
        <div class="col-md-4 col-10 mb-5 mr-auto ml-auto">
            <div class="card Books rounded">
                <div class="card-body text-light">
                    <i class="fas fa-book"></i>
                    <h5 class="card-title ">Books</h5>
                    <p class="card-text ">
                        <span class="number"> <?php echo count($this->BOOKS);?> </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-10 mb-5 mr-auto ml-auto">
            <div class="card Users rounded">
                <div class="card-body text-light">
                    <i class="fas fa-users"></i>
                    <h5 class="card-title">Users</h5>
                    <p class="card-text ">
                        <span class="number"> <?php echo count($this->DATA) - 1;?> </span>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-10 mb-5 mr-auto ml-auto">
            <div class="card Views rounded">
                <div class="card-body text-light">
                    <i class="fas fa-eye"></i>
                    <h5 class="card-title">Views</h5>
                    <p class="card-text ">
                        <span class="number"> <?php echo getViews ();?> </span>
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>