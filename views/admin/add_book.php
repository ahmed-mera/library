<style>
    body{
        background-color:#EEE;
        padding-top: 55px
    }
</style>

    <form action="index.php?req=new_book" method="post" enctype="multipart/form-data" class='form-add-book'>
        <h2 class = 'h1 text-center mb-4 mt-0'> add a new book </h2> 
        <input type="hidden" name="id" value="<?php echo $this->model_admin->next_id ?>" />
        <input type="hidden" name="add_new_book" />
        <div class="form-group">
            <input type="text" class="form-control" name="bookName" placeholder="Book Name" required value="<?php echo (!empty($dataBook [0]))? $dataBook [0] : ''; ?>">
            <span class = 'require'>*</span>
        </div>  
        <div class="form-group">
            <input type="text" class="form-control" name="writer" placeholder=" Writer " required value="<?php echo (!empty($dataBook [3]))? $dataBook [3] : ''; ?>">
            <span class = 'require'>*</span>
        </div>
        <div class="form-group ">
            Languages :
            <div class="custom-control custom-checkbox d-md-inline d-block ml-md-5 ml-2">
                <input type="checkbox" class="custom-control-input" name='lang[]' id="customCheck" value="Italian" checked>
                <label class="custom-control-label" for="customCheck">Italian</label>
            </div>
            <div class="custom-control custom-checkbox d-md-inline d-block ml-md-5 ml-2 ">
                <input type="checkbox" class="custom-control-input" name='lang[]' id="customCheck1" value="English">
                <label class="custom-control-label" for="customCheck1">English</label>
            </div>
            <div class="custom-control custom-checkbox d-md-inline d-block  ml-md-5 ml-2">
                <input type="checkbox" class="custom-control-input" name='lang[]' id="customCheck2" value="Arabic">
                <label class="custom-control-label" for="customCheck2">Arabic</label>
            </div>
            <span class = 'require'>*</span>
        </div>
        <!-- <div class="form-group"> d-md-inline d-block
            <input type="text" class="form-control" name="lang" placeholder=" Language " required value="<?php //echo (!empty($dataBook [2]))? $dataBook [2] : ''; ?>">
            <span class = 'require'>*</span>
        </div> -->
        <div class="form-group">
            <input type="number" class="form-control" name="available" placeholder=" Quantity Avialble "  min="1" max="100" required value="<?php echo (!empty($dataBook [5]))? $dataBook [5] : ''; ?>">
            <span class = 'require'>*</span>
        </div>
        <div class="custom-file mb-4">
            <input type="file" class="custom-file-input"  name="coverBook" id="validatedCustomFile" accept="image/png, image/jpeg, image/jpg, image/git, image/webp">
            <label class="custom-file-label" for="validatedCustomFile">Choose a cover book</label>
        </div>
        <div class="form-group">
            <textarea class="form-control" name="description" placeholder=" Description " rows='7' cols='50'><?php echo (!empty($dataBook [4]))? $dataBook [4] : ''; ?></textarea>
        </div>
        <div class="form-group">
            <button type = 'submit' class='btn btn-primary'> Add A new book </button>  
        </div>
    </form>
