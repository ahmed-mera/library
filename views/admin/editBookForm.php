<style>
    body{
        background-color:#EEE;
        padding-top: 50px;
    }
    .form-add-book > h2 {
        letter-spacing: -1px
    }
</style>

<?php
$lang = explode('-',$this->BOOKS[$book]['lang']);
// $this->BOOKS[$book]['description'] = (!empty($this->BOOKS[$book]['description'])) ? explode(' ',$this->BOOKS[$book]['description']) : '';

?>
    <form action="index.php?req=update_book" method="post" enctype="multipart/form-data" class='form-add-book form-update-book'>
        <h2 class = 'h1 text-center mb-4 mt-0'> update book </h2> 
        <input type="hidden" name="id" value="<?php echo $this->BOOKS[$book]['id']?>" />
        <input type="hidden" name="update_book" />
        <div class="form-group">
            <input type="text" class="form-control" name="bookName" placeholder="Book Name" required value="<?php echo (!empty($this->BOOKS[$book]['name_book']))? $this->BOOKS[$book]['name_book'] : ''; ?>">
            <span class = 'require'>*</span>
        </div>  
        <div class="form-group">
            <input type="text" class="form-control" name="writer" placeholder=" Writer " required value="<?php echo (!empty($this->BOOKS[$book]['writer']))? $this->BOOKS[$book]['writer'] : ''; ?>">
            <span class = 'require'>*</span>
        </div>
        <div class="form-group ">
            Languages :
            <div class="custom-control custom-checkbox d-inline ml-5">
                <input type="checkbox" class="custom-control-input" name='lang[]' id="customCheck" value="Italian" <?php echo (in_array('italian',$lang))? 'checked': ''; ?> >
                <label class="custom-control-label" for="customCheck">Italian</label>
            </div>
            <div class="custom-control custom-checkbox d-inline ml-5 ">
                <input type="checkbox" class="custom-control-input" name='lang[]' id="customCheck1" value="English" <?php echo (in_array('english',$lang))? 'checked': ''; ?> >
                <label class="custom-control-label" for="customCheck1">English</label>
            </div>
            <div class="custom-control custom-checkbox d-inline ml-5">
                <input type="checkbox" class="custom-control-input" name='lang[]' id="customCheck2" value="Arabic" <?php echo (in_array('arabic',$lang))? 'checked': ''; ?> >
                <label class="custom-control-label" for="customCheck2">Arabic</label>
            </div>
            <span class = 'require'>*</span>
        </div>
        <!-- <div class="form-group">
            <input type="text" class="form-control" name="lang" placeholder=" Language " required value="<?php //echo (!empty($dataBook [2]))? $dataBook [2] : ''; ?>">
            <span class = 'require'>*</span>
        </div> -->
        <div class="form-group">
            <input type="number" class="form-control" name="available" placeholder=" Quantity Avialble "  min="1" max="100" required value="<?php 
            echo (!empty($this->BOOKS[$book]['available']))? $this->BOOKS[$book]['available'] : ''; ?>">
            <span class = 'require'>*</span>
        </div>
        <div class="custom-file mb-4">
            <input type="file" class="custom-file-input"  name="coverBook" id="validatedCustomFile" accept="image/png, image/jpeg, image/jpg, image/git, image/webp">
            <label class="custom-file-label text-danger" for="validatedCustomFile" >if you want to change the current Cover book </label>
        </div>
        <input type="hidden" name="src_img" value="<?php echo  $this->BOOKS[$book]['src_img'] ;//cover book ?> ">
        <div class="form-group">
            <textarea class="form-control" name="description" placeholder=" Description " rows='7' cols='50'><?php
            if (!empty($this->BOOKS[$book]['description'])){ echo $this->BOOKS[$book]['description']; } ?></textarea>
        </div>
        <div class="form-group">
            <button type = 'submit' class='btn btn-primary update_book'> Save </button>  
        </div>
    </form>


<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
