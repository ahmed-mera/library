<style>
    body{
        background-color:#EEE;
        padding-top: 55px
    }
</style>

    <form action="index.php?req=new_user" method="post" enctype="multipart/form-data" class='form-add-book'>
        <h2 class = 'h1 text-center mb-4 mt-0'> add a new user </h2> 
        <input type="hidden" name="add_new_user" />
        <div class="form-group">
            <input type="text" class="form-control" name="fullname" placeholder="Full Name" required autocomplete="off"
                    value="<?php echo (!empty($dataUser [0]))? $dataUser [0] : ''; ?>">
            <span class = 'require'>*</span>
        </div>  
        <div class="form-group">
            <input type="email" class="form-control" name="username" placeholder=" usename ( Email ) " required  autocomplete="off"
                    value="<?php echo (!empty($dataUser [1]))? $dataUser [1] : ''; ?>">
            <span class = 'require'>*</span>
        </div>
        <div class="input-group form-group mb-3">
            <span class="privileges"> Select privileges : </span> 
            <select class="custom-select" id="inputGroupSelect01" name="privileges">
                <option value="0" selected>User</option>
                <option value="1">Admin</option>
            </select>
            <span class = 'require'>*</span>
        </div>
        <div class="form-group">
            <input type="password" class="form-control  pass-login" name="password" placeholder=" password "  required 
                    value="<?php echo (!empty($dataUser [2]))? $dataUser [2] : ''; ?>"  autocomplete="new-password">
            <i class="fas fa-eye show-pass add-user"></i>
            <span class = 'require'>*</span>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="confirm-password" placeholder=" Confirm Password " autocomplete="new-password"  required >
            <span class = 'require'>*</span>
        </div>

        <div class="form-group">
            <button type = 'submit' class='btn btn-primary'> Add A new user </button>  
        </div>
    </form>
