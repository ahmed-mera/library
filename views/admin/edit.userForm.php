<style>
    body{
        background-color:#EEE;
        padding-top: 50px;
    }
    .form-add-book > h2 {
        letter-spacing: -1px
    }
</style>

<?php

?>
    <form action="index.php?req=update_user" method="post" enctype="multipart/form-data" class='form-add-book form-update-book'>
        <h2 class = 'h1 text-center mb-4 mt-0'> update user </h2> 
        <input type="hidden" name="id-arr" value="<?php echo $_REQUEST['id']?>" />
        <input type="hidden" name="id" value="<?php echo $this->DATA[$id][3]?>" />
        <input type="hidden" name="update_user" />
        <div class="form-group">
            <input type="text" class="form-control" name="fullname" placeholder="Full Name" required
            value="<?php echo (!empty($this->DATA[$id][0]))? $this->DATA[$id][0] : ''; ?>">
        </div>  
        <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder=" Email ( user name ) " required 
            value="<?php echo (!empty($this->DATA[$id][1]))? $this->DATA[$id][1] : ''; ?>">
        </div>
        <div class="form-group">
            <input type="password" class="form-control pass-login" name="password" placeholder=" Password ( password ) " value="">
            <i class="fas fa-eye show-pass add-user"></i>
        </div>
        <div class="input-group form-group mb-3">
            <span class="privileges"> Select privileges : </span> 
            <select class="custom-select" id="inputGroupSelect01" name="privileges">
                <option value="0" <?php echo ( $this->DATA[$id][4] == 0 )? 'selected': ''; ?> >User</option>
                <option value="1" <?php echo ( $this->DATA[$id][4] == 1 )? 'selected': ''; ?>>Admin</option>
            </select>
        </div>
        <div class="form-group">
            <button type = 'submit' class='btn btn-primary update_book'> Save </button>  
        </div>
    </form>


<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------------------- -->
