<style>

        
</style>
    <div class="container mx-auto text-center  ">
        <h1 class="text-center edit-books mt-3 mb-5 ">Edit Or Delete</h1>
        <div class="row users " >
                <div class="col-2"><h4> ID   </h4></div>
                <div class="col-3"><h4> Name </h4></div>
                <div class="col-5"><h4> Email </h4></div>
                <div class="col-2"><h4> Type  </h4></div>
                <!-- <div class="col-3">Action</div> -->
            <?php  for ($i = 0 ; $i < count($this->DATA) -1 ; $i++ ):
                    $this->DATA[$i][4] = ( $this->DATA[$i][4] == 1) ? ' Admin ' : ' User ';

                ?>
                    <div class= "col-12"><hr /></div>
                    <div class="col-12  position-relative data-user  pb-5">
                        <div class="row pt-5 pb-5 ">
                            <div class= "col-2 lead"><?php echo $this->DATA[$i][3] ?></div> <!-- ID-->
                            <div class= "col-3 lead"><?php echo $this->DATA[$i][0] ?></div> <!-- NAME-->
                            <div class= "col-5 lead"><?php echo $this->DATA[$i][1] ?></div> <!-- EMAIL-->
                            <div class= "col-2 lead"><?php echo $this->DATA[$i][4] ?></div> <!-- PREVILEGES-->
                            <div class= "col-12 action lead">
                            <a href="?req=update_user_admin&id=<?php echo $i  ?>" class='edit-user-link' data-id ="<?php echo $i  ?>">
                                <button class="btn btn-primary edit btn-edit-user mt-3 mb-3">
                                        <i class="fas fa-pen"></i>
                                        Edit
                                </button>
                            </a>
                            <a href="#" class='delete-user-link' data-id ="<?php echo $i  ?>">
                                <button class="btn btn-danger btn-delete btn-delete-user mt-3 mb-3">
                                        Delete
                                        <i class="fas fa-trash-alt"></i>
                                </button>
                            </a>
                            </div> <!-- actions-->
                        </div>
                    </div>
            <?php  endfor; ?>
        </div>
    </div>

