<style>
    body{background-color:#EEE; padding-top: 10px !important}
</style>
<form action="index.php?req=sign_up" method="post" enctype="multiparti/data-form" class='form-sign-up'>
    <h1 class="text-center mb-5" style="letter-spacing:-1px">Register</h1>
    <input type="hidden" name="sign_up"  />
    <div class="form-group">
        <input type="text" class="form-control" name="fullName" placeholder="Your Full Name" autocomplete="off" value="<?php echo (isset($data[0])) ? $data[0]: ''; ?>" required>
    </div>
    <div class="form-group">
        <input type="email" class="form-control" name="email" placeholder="Your User Name ( E-mail )" autocomplete="off" value="<?php echo (isset($data[1])) ? $data[1]: ''; ?>"required>
    </div>
    <div class="form-group">
        <input type="password" class="form-control pass-sign-up" name="password" placeholder="Your Password" autocomplete="new-password" value="<?php echo (isset($data[2])) ? $data[2]: ''; ?>" required>
        <small class="form-text text-muted information">
            Your password must be 6-10 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
        </small>
    </div>
    <div class="form-group">
        <input type="password" class="form-control" name="confirm-password" placeholder="Confirm Password" autocomplete="new-password" required>
    </div>
    <button type="submit" class='btn btn-primary btn-block'> Submit </button>  
    <a href="?req=login" class="d-block mt-5">  Have an account ? </a>
    <a href="index.php" class="d-block mt-3"> <i class="fas fa-home fa-lg"></i>  home </a>
</form>

