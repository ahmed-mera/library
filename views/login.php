<style>
    body{background-color:#EEE; padding-top: 10px !important}
</style>

<div class="container">
    <fieldset class="mr-auto ml-auto" <?php echo (isset($_COOKIE['hack-login']) && $_COOKIE['hack-login'] == 5 ) ? 'disabled' : ''; ?> >
        <form action="index.php?req=login_in" method="post" enctype="multiparti/data-form" class='form-login'>
            <h1 class="text-center mb-5" style="letter-spacing:-1px">Login</h1>
            <input type="hidden" name="login"  />
            <div class="form-group">
                <input type="email"
                class="form-control" 
                name="email" placeholder="Your User Name ( E-mail )" 
                autocomplete="off"
                value="<?php 
                if (isset($username)){ echo $username ;} 
                else if (isset($_COOKIE['user'])) {echo $_COOKIE['user'] ;}
                ?> "required>
            </div>
            <div class="form-group">
                <input type="password" class="form-control pass-login" name="password" placeholder="Your Password" autocomplete="new-password"  required>
                <i class="fas fa-eye show-pass"></i>
            </div>
            <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" name='rememberMe' class="custom-control-input" id="customCheck1" value="checked">
                <label class="custom-control-label" for="customCheck1">Remember Me</label>
            </div>
            <button type="submit" class='btn btn-primary btn-block'> Submit </button> 
            <a href="?req=register" class="d-block mt-5"> create a new account  </a>
            <a href="?req=forgotPassword" class="d-block mt-3"> Forgot Password </a>
            <a href="index.php" class="d-block mt-3"> <i class="fas fa-home fa-lg"></i>  home </a> 
        </form>
    </fieldset> 
</div>