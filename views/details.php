
<style>.new{width: 100px}


</style>

    <div class="container position-relative">
        <div class= 'alert alert-info position-fixed container mx-auto pop'>
            <p class="lead mx-auto text-center">
                you must login or register for booking this book
                <button class="btn btn-danger btn-block mt-5 mb-1">Ok</button>
            </p>
        </div>
        <div class="row">
            <div class="col-5 text-center image-responsive position-relative">
            <img src="views/images/new.png" alt="new"  class="img-thumbnail rounded mx-auto ml-3 new">
                <img
                    src="<?php  echo $this->books[$this->request_book]['src_img'] ?>" 
                    alt="<?php  echo $this->books[$this->request_book]['name_book'] ?>"
                    data-expire="<?php  echo $this->books[$this->request_book]['expire_date'] ?>"
                    class="img-thumbnail rounded mx-auto d-block">
            </div>
            <div class="col-7">
                <div class="pt-md-5 pt-2">
                    <?php
                    echo '<p class="lead"> <strong class="details"> Book Name </strong>: ' .ucwords( $this->books[$this->request_book]['name_book']).'</p>'
                        .'<p class="lead"> <strong class="details"> Writer </strong>: ' . ucwords($this->books[$this->request_book]['writer']).'</p>'
                        .'<p class="lead"> <strong class="details"> Languages </strong>: ' ;
                        $this->books[$this->request_book]['lang'] = explode('-',$this->books[$this->request_book]['lang']);
                        foreach ($this->books[$this->request_book]['lang'] as $lang) {
                            if(end($this->books[$this->request_book]['lang']) != $lang):
                                echo  ucwords($lang) .' , ';
                            else:
                                echo  ucwords($lang);
                            endif;
                        }
                        echo '</p>'
                        .'<p class="lead"> <strong class="details"> Availbale</strong> : ' . $this->books[$this->request_book]['available'].' Books</p>'
                        .'<p class="lead"> <strong class="details"> Description </strong>: ' ;
                            echo ($this->books[$this->request_book]['description'] != null)?
                            ucwords($this->books[$this->request_book]['description']) :
                            'Not Available';
                    echo'</p>
                        <a href="index.php?req=prenota&name='.trim($this->books[$this->request_book]['name_book']).'" 
                        class="badge badge-danger text-uppercase mr-2 mb-3 pt-2 pb-2 pl-auto pr-auto BOOK ';
                        if(! isset($_SESSION['user_name'])){ echo 'NO-BOOK'; } 
                        echo '" data-name-book='.$this->books[$this->request_book]['name_book'].'>
                            <i class="fas fa-bookmark fa-lg mr-1"></i>
                            Book
                        </a>';
                    ?>
                    <a href="index.php" class="badge badge-primary text-uppercase pt-2 pb-2 pl-auto pr-auto ">
                        <i class="fas fa-home fa-lg mr-1"></i>
                        home
                    </a>

                </div>
            </div>
        </div>
    </div>


