<style>body{ background: #f7f7f7 ; padding-top: 68px !important } </style>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
        <div class="carousel-item active">
                <img src="views/images/carousel/4.png" class="d-block w-100" alt="brain">
            </div>  
            <div class="carousel-item">
                <img src="views/images/carousel/2.png" class="d-block w-100" alt="book_2">
            </div>
            <div class="carousel-item">
                <img src="views/images/carousel/3.jpg" class="d-block w-100" alt="book_1">
            </div>
            <div class="carousel-item ">
                <img src="views/images/carousel/1.jpg" class="d-block w-100" alt="book_3">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
        </a>
    </div>
    <div class='container'>  
        <div class='row'>  
            <?php
                //here get all books
                //here put them in anchor[ link ]
                foreach ($this->books as $key => $value): ?>
                    <div class="col-sm-3 col-6 mr-sm-0 ml-sm-0 mr-auto ml-auto mt-4 mb-4 text-center position-relative content">
                        <a href="index.php?req=<?php echo $this->books[$key]['name_book'].'&id='.$this->books[$key]['id'] ?>">  
                            <img src="views/images/new.png" alt="new"  class="img-thumbnail rounded mx-auto new">
                            <img src="<?php echo $this->books[$key]['src_img'] ?>" 
                                alt="<?php echo $this->books[$key]['name_book'] ?>" 
                                data-expire="<?php  echo $this->books[$key]['expire_date']?>"
                                class="img-thumbnail rounded mx-auto d-block">
                            <div class="content-text mt-2 text-center text-dark">
                                    <?php echo ucwords( $this->books[$key]['name_book'] ) ?>
                                </div>
                            </a>
                        </div> 
            <?php  endforeach  ?>
        </div>
    </div>


