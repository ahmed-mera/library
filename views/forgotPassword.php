<style>
    body{background-color:#EEE; padding-top: 10px !important}
</style>

<div class="container">
    <fieldset class="mr-auto ml-auto" <?php echo (isset($_COOKIE['hack-forgot-password']) && $_COOKIE['hack-forgot-password'] == 5 ) ? 'disabled' : ''; ?> >
        <form action="?req=forgotPasswordCheck" method="post" enctype="multiparti/data-form" class='form-forget-password'>
            <h1 class="text-center mb-5" style="letter-spacing:-1px">Forgot Password</h1>
            <input type="hidden" name="forgotPasswordCheck"  />
            <div class="form-group">
                <input type="text"class="form-control" name="fullname" placeholder=" Your Full name"  autocomplete="off" value="<?php echo (isset($data[0])) ? $data[0]: ''; ?>" required>
            </div>
            <div class="form-group">
                <input type="email"class="form-control" name="email" placeholder="Your User Name ( E-mail )"  autocomplete="off" value="<?php echo (isset($data[1])) ? $data[1]: ''; ?>"  required>
            </div>
            <div class="form-group">
                <input type="password" class="form-control pass-forgot" name="password" placeholder="New Password" autocomplete="new-password" value="<?php echo (isset($data[2])) ? $data[2]: ''; ?>"   required>
                <small class="form-text text-muted information">
                    Your password must be 6-10 characters long, contain letters and numbers, and must not contain spaces, special characters, or emoji.
                </small>
            </div>
            <div class="form-group">
                <input type="password" class="form-control pass-login" name="confirm-password" placeholder="Confirm Password" autocomplete="new-password"  required>
            </div>
            <button type="submit" class='btn btn-primary btn-block'> Submit </button> 
            <a href="index.php" class="d-block mt-4"> <i class="fas fa-home fa-lg"></i>  home </a> 
        </form>
    </fieldset> 
</div>