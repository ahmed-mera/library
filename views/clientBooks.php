<div class="container text-center mx-auto mt-5">
    <div class="row">
    <?php   $booksClient = $this->model->readFileClient();
                if(!empty($booksClient)){ ?>
        <div class="col-12 title">
            <div class="row">
                <div class="col-md-3 d-lg-block d-none mb-5"> <h4> <?php echo 'Cove Image ' ?> </h4></div>
                <div class="col-md-3 d-lg-block d-none col-6 mb-5"> <h4> <?php echo  'Book Name'  ?> </h4></div>
                <div class="col-md-3 d-lg-block d-none col-6 mb-5"> <h4> <?php echo 'Date Booked ' ?> </h4></div>
                <div class="col-md-3 d-lg-block d-none col-6 mb-5"> <h4> <?php echo 'Date Expire ' ?> </h4></div>
            </div>
        </div>
        <?php   foreach ($booksClient as $key => $value):
                        $booksClient[$key]['expire_date'] = ceil( ( $booksClient[$key]['expire_date'] - time()  ) / (60 * 60 * 24 * 1) + 2 );
        ?>      <div class="col-12 line"> <hr /> </div>
                <div class="col-12 informationBook position-relative">
                    <div class="row mb-5">
                        <div class="col-md-3 col-10 "><img class="mt-3 img-thumbnail rounded mx-auto ml-3 booksClient"
                        src=" <?php echo $booksClient[$key]['src_img']  ?> " 
                        style="height:200px"></div>
                        <div class="col-md-3 col-10 info"> <?php echo ucwords( $booksClient[$key]['name_book']) ?> </div>
                        <div class="col-md-3 col-10 info"> <?php echo ucwords( $booksClient[$key]['date']) ?> </div>
                        <div class="col-md-3  col-10 info"> <?php echo 'missing <strong style="color:var(--danger)">'. $booksClient[$key]['expire_date'].'</strong>  days' ?> </div>
                    </div>
                    <a href="#" data-name_book="<?php echo $booksClient[$key]['name_book'] ?>"  class="delete">
                        <button class="btn btn-danger text-left postion-absolute"><i class="fa fa-trash-alt"></i> Delete</button>
                    </a>
                </div>
        <?php endforeach; } else {?>
            <div class="col-12 mx-auto alterText-2">
                    <p class="lead">No Books ;) </p>
            </div>
        <?php } ?>
        <div class="col-12 mx-auto alterText">
                    <p class="lead">No Books ;) </p>
        </div>
    </div>
</div>