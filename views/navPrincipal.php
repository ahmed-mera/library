<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
    <div><a class="navbar-brand logo" href="index.php"> <span></span><span>AM</span> </a> </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars "></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php"><i class="fas fa-home fa-lg"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?req=blog">blog</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?req=about">about us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?req=contact">contact us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link login" href="?req=login"> <i class="fas fa-user"></i> login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link register" href="?req=register"> <i class="fas fa-user-plus"></i> register</a>
                </li>
                <li class="nav-item">
                    <span class="nav-link search">
                            <i class="fas fa-search icon-search"></i>
                    </span>
                </li>
            </ul>
        </div>
    </div>
</nav>
<form action="?req=search">
    <input type="search" name="search" class="form-control form-control-lg input-search position-fixed" placeholder="Enter the name of book ">
    <div class="ris-search"></div>
</form>