<?php
        $value = getViews ();
        if (!isset($_COOKIE['viewer'])) {
            global $value ;
            setcookie('viewer' , $value, time() + 2592000, '/' , FALSE, FALSE);
            $value++;
            setViews ($value);
        }
            function setViews($val){
                $fp = fopen('models/data/views.txt', 'w');
                fprintf($fp, '%d', $val);
                fclose($fp);
            }
    
            function getViews(){
                $fp = fopen('models/data/views.txt', 'r');
                fscanf($fp, '%d', $val);
                fclose($fp);
                return $val;
            }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="../page3/images/logo.red.pieno.png">
    <title>Mera library </title>
    <script src="../layout/js/jquery-3.3.1.min.js"></script> <!-- jquery-->
    <script src="../layout/js/bootstrap.min.js"></script> <!-- bootsrap-->
    <link rel="stylesheet" href="../layout/css/bootstrap.min.css"> <!-- bootsrap-->
    <link rel="stylesheet" href="../layout/css/all.min.css"> <!-- fontawosme-->
    <link rel="stylesheet" href="./views/css/main.css"> <!-- my css-->
</head>
<body>