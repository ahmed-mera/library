
<?php
ob_start();//for header
//start session
session_start(); 
//use namespace for root [ admin ]
use controller\admin as controller_admin;
//use the namespace for all user
use controller as controller;
//include the header
require 'views/header.php';
#-------------------------------------------------------------------------------------------------------------------------
/* NOTE: for admin */

//check if is session or not
if (isset($_SESSION['privileges']) && $_SESSION['privileges'] == 1) { //for admins
    //include the class controller
    require __DIR__.'/controllers/controller.admin.php';
    require 'views/admin/nav.admin.php';
    //create the new object
    $controller = new controller_admin\controller();
    //call the function invoke of class controller
    $controller->__invoke();
} 

#-------------------------------------------------------------------------------------------------------------------------
/* NOTE: for user registered */

else if (isset($_SESSION['privileges']) && $_SESSION['privileges'] == 0) {//for user registered 
    //include the class controller
    require __DIR__.'/controllers/controller.php';
    //create the new object
    $controller = new controller\controller();
    //call the function invoke of class controller
    $controller->__invoke();
    if(($controller->nav == 'nav')){ require 'views/nav.user.php'; }

}

#-------------------------------------------------------------------------------------------------------------------------
/* NOTE: for user no registered */

else{//for user no registered 
    //include the class controller
    require __DIR__.'/controllers/controller.php';
    //create the new object
    $controller = new controller\controller();
    //call the function invoke of class controller
    $controller->__invoke();
    //include the nav
    if(($controller->nav == 'nav')){ require 'views/navPrincipal.php';}
}
//include the footer
require "views/footer.php";
ob_end_flush();//for header