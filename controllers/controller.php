<?php
//the namespace of control
namespace controller;
//include the directory model
require 'models/model.php';

//namespace of model
use model as model;



/**
 ** class which have all things of controls
 **  this class mange all my application web
 **/
class controller {

    //declare variables 
    private $request;
    private $model;
    protected $books;
    protected $request_book;
    public $nav = 'nav';
    // public $footer = 'footer';
    // public $header = 'header';

    //the construct of this class, assignment to the variable request the default value if has not it
    public function __construct() {
        //class model
        $this->model = new model\model();
        $this->books = $this->model->getBooks();
        $this->request = 'all_books';
        // foreach ($this->books as $name => $value) {
        //     $this->nameBook [] = $name ;
        // }
        // echo $this->nameBook;
        // echo json_decode($this->nameBook);
        
    }

    //this function will be called by index.php to invoke the class control
    public function __invoke()
    {
        //here control if isset request o no
        if (isset($_REQUEST["req"])):

            //assign to the variable request the value of the request
            $this->request = $_REQUEST["req"];
            $count = 0;
            //check the book name by my array
            foreach ($this->books as $key => $value) :
                    //check the book name
                if($this->request == $key ):
                    //assign the value of this variable
                    $this->request_book = $key;
                    //call the page whose the information of this book
                    require "./views/details.php";
                else:
                    //if dose not exist i will increment this variable
                    $count++;
                endif;
            endforeach;
            switch ( $this->request):
                case 'login':
                if(isset($_SESSION['user_name'])){
                    $this->model->redirect('',0,'index.php');
                }else{
                    $this->nav = ' noNav';
                require "views/login.php";
                }
                break;
                case 'login_in':
                $this->nav = ' noNav';
                //check if method = post
                if(isset($_POST['login'])){
                    //filtering and transform all in lower case
                    $username = trim( strtolower( filter_var ( $_POST['email']  , FILTER_SANITIZE_EMAIL ) ) );
                    $password =  filter_var ( $_POST['password'], FILTER_SANITIZE_STRING ) ;
                    $remember =  (isset( $_POST['rememberMe'] ) && !empty($_POST['rememberMe']) ) ? $username : '';
                    if ($this->model->login($username,$password,$status)) {
                            //all right 
                            //echo $remember;
                            setcookie('user',$remember,time()+ (86400 * 7 ),'/');
                            $this->model->redirect('',0,'index.php');
                    } else {
                        if(! isset($_SESSION['hack-login'])){
                            $_SESSION['hack-login'] = 1;
                        }else{
                            if($_SESSION['hack-login'] >= 4){
                                setcookie('hack-login',$_SESSION['hack-login'] + 1,time() + (1 * 60), '/');//for 1 minute
                                $_SESSION['hack-login'] = 0;
                            }
                            $_SESSION['hack-login']++;
                        }  
                        if(isset($_COOKIE['hack-login']) && $_COOKIE['hack-login'] == 5){
                            echo'  <div class="alert alert-warning alert-dismissible fade show alert-login" role="alert">
                                        <p class="lead"> You must wait 1 minute before trying again  
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </p>
                                    </div> ';
                        }else{
                            echo'  <div class="alert alert-danger alert-dismissible fade show alert-login" role="alert">'
                                        .$status.
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div> ';
                        }
                        require "views/login.php";
                    }
                    
                }else{ $this->model->redirect('',0,'?req=login');} //redirect
                break;
                case 'register':
                    $this->nav = ' noNav';
                    require "views/register.php";
                break;
                case 'sign_up':
                $this->nav = ' noNav';
                //check if method = post
                if(isset($_POST['sign_up'])){
                    //filtering and transform all in lower case
                    $data [] = trim( strtolower( filter_var ( $_POST['fullName'], FILTER_SANITIZE_STRING ) ) );
                    $data [] = trim( strtolower( filter_var ( $_POST['email']  , FILTER_SANITIZE_EMAIL ) ) );
                    $data [] =  filter_var ( $_POST['password'], FILTER_SANITIZE_STRING );
                    $data [] =  filter_var ( $_POST['confirm-password'], FILTER_SANITIZE_STRING ) ;
                    if ($this->model->register($data,$status)) {
                        $msg = '  <div class="alert alert-success alert-dismissible fade show alert-sign-up" role="alert"> 
                                        <p class="lead">
                                            <strong> Congratulation</strong>, now you can login you will redirect at your Home
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </p>
                                </div>
                            ';
                        $this->model->redirect($msg,5,'?req=login');
                    } else {
                        echo'  <div class="alert alert-danger alert-dismissible fade show alert-sign-up" role="alert">
                                    <p class="lead">';
                                        foreach ($status as $errors) { echo $errors; }
                                        echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                    </p>
                                        </button>
                                </div> ';
                        require "views/register.php";
                    }
                    
                }else{ $this->model->redirect('',0,'?req=register');} //redirect
                break;
                case 'forgotPassword':
                    $this->nav = ' noNav';
                    require 'views/forgotPassword.php';
                break;
                case 'forgotPasswordCheck':
                    $this->nav = ' noNav';
                //check if method = post
                if(isset($_POST['forgotPasswordCheck'])){
                    //filtering and transform all in lower case
                    $data [] = trim( strtolower( filter_var ( $_POST['fullname'], FILTER_SANITIZE_STRING ) ) );
                    $data [] = trim( strtolower( filter_var ( $_POST['email']  , FILTER_SANITIZE_EMAIL ) ) );
                    $data [] =  filter_var ( $_POST['password'], FILTER_SANITIZE_STRING ) ;
                    $data [] =  filter_var ( $_POST['confirm-password'], FILTER_SANITIZE_STRING ) ;
                    if ($this->model->forgotPassword($data,$status)) {
                        $msg = '  <div class="alert alert-success alert-dismissible fade show alert-sign-up" role="alert"> 
                                        <p class="lead">
                                            <strong> Congratulation</strong>, Password is updated with successfully
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </p>
                                </div>
                            ';
                        $this->model->redirect($msg,3,'?req=login');
                    }else {
                        if(! isset($_SESSION['hack-forgot-password'])){
                            $_SESSION['hack-forgot-password'] = 1;
                        }else{
                            if($_SESSION['hack-forgot-password'] >= 4){
                                setcookie('hack-forgot-password',$_SESSION['hack-forgot-password'] + 1,time() + (1 * 60), '/');//for 1 minute
                                $_SESSION['hack-forgot-password'] = 0;
                            }
                            $_SESSION['hack-forgot-password']++;
                        }  
                        if(isset($_COOKIE['hack-forgot-password']) && $_COOKIE['hack-forgot-password'] == 5){
                            echo'  <div class="alert alert-warning alert-dismissible fade show alert-login" role="alert">
                                        <p class="lead"> You must wait 1 minute before trying again  
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </p>
                                    </div> ';
                        }else{
                            echo'  <div class="alert alert-danger alert-dismissible fade show alert-login" role="alert">';
                                        foreach ($status as $errors) { echo $errors; }
                            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div> ';
                        }
                        require "views/forgotPassword.php";
                    } 
                }else{ $this->model->redirect('',0,'index.php');} //redirect
                break;
                case 'prenota':
                    //for request js 
                    $this->nav = ' noNav';//navbar 
                    // $this->footer = ' noFooter';//footer
                    // $this->header = ' noHeader';//header

                    $nameBook = $_REQUEST['name'];
                    $ClientBooks = $this->model->readFileClient();
                    //check  the book if client have it o no
                    if(\is_array($ClientBooks)):
                        foreach ($ClientBooks as $key => $value):
                            if(strtolower($nameBook) == strtolower($key)){
                                $msg = '  <div class="alert alert-danger alert-dismissible fade show alert-sign-up book-exists " role="alert">
                                            <p class="lead"> this book already exists in your books.
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                </button>
                                            </p>
                                        </div>';
                                        $this->model->redirect($msg,3,$_SERVER['HTTP_REFERER']);
                                return false;

                            }
                        endforeach;
                    endif;
                    foreach ($this->books as $key => $value):
                        if(strtolower($nameBook) == strtolower( $this->books[$key]['name_book'])){
                            $book  =  $this->books[$key];
                            $book['date'] = date("d/m/Y - H:i:s",time());
                            $book['expire_date'] = strtotime(date('Y-m-d h:i:s',time()));
                            unset($book['writer']);//delete the writer
                            unset($book['lang']);//delete the lang
                            unset($book['description']);//delete the description
                            unset($book['available']);//delete the avaliable
                            unset($book['id']);//delete the id
                            
                            $this->model->prenota($book);//save it
                            // echo count($ClientBooks) + 1;//if it true return number of books
                            $msg = '  <div class="alert alert-success alert-dismissible fade show alert-sign-up book-noExists " role="alert">
                                        <p class="lead"> book is booked with successfully, <mark>this book will be cancelled after 2 days</mark>  if you won\'t take it.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                            </button>
                                        </p>
                                    </div>';
                                    $this->model->redirect($msg,4,$_SERVER['HTTP_REFERER']);
                            return true;
                            //$this->model->redirect('',4,$_SERVER['HTTP_REFERER']);
                        }
                    endforeach;
                break;
                case 'booked':
                    require 'views/clientBooks.php';
                break;
                case 'del-book':
                $this->nav = ' noNav';//navbar 
                    if(isset($_POST['name'])):
                        $bookName = $_POST['name'];
                            $this->model->deleteBook($bookName);
                    endif;
                break;
                case 'logout':
                    session_unset();
                    session_destroy();
                    $this->model->redirect('',0,'index.php');
                    exit;
                break;
                default:
                    //check if the book name dose not exist
                    ($count === count($this->books))? require "views/all_books.php": null;
                break;
            endswitch;
        else:
            //if not exists the request i will include all books
            require "views/all_books.php";
        endif;
    }
}

