<?php
//the namespace of control
namespace controller\admin;
//include the directory model
require 'models/model.admin.php';

//namespace of model admin
use model\admin as model_admin;


/**
 ** class which have all things of controls
 **  this class mange all my application web
 **/
class controller{

    //declare variables 
    private $request;
    private $model_admin;
    private $BOOKS ;
    private $DATA;

    //the construct of this class, assignment to the variable request the default value if has not it
    public function __construct() {
        //class model
        $this->model_admin = new model_admin\model();
        $this->BOOKS = $this->model_admin->getBooks();
        $this->DATA = $this->model_admin->readDataClient();
        $this->request = 'dashboard';
    }

    //this function will be called by index.php to invoke the class control
    public function __invoke()
    {
        //here control if isset request o no
        if (isset($_REQUEST["req"])):

            //assign to the variable request the value of the request
            $this->request = $_REQUEST["req"];
            //controll the request
            switch ( $this->request):
                case 'add_book':
                    require 'views/admin/add_book.php';
                break;
                case 'new_book':
                    if(isset($_POST['add_new_book'])){
                        $order   = array("\r\n", "\n", "\r");
                        //here i should cll the function form model
                        //prepare the data
                        $dataBook [] =  (!empty($_REQUEST['bookName']))    ?  strtolower ( filter_var( $_REQUEST['bookName']   , FILTER_SANITIZE_STRING     )):  NULL ; //require 0
                        $dataBook [] = '';//for cover book 
                        $dataBook [] =  (!empty($_REQUEST['lang']))        ?  strtolower ( filter_var( implode('-', $_REQUEST['lang'])       , FILTER_SANITIZE_STRING    )):  NULL ; //require 2 
                        $dataBook [] =  (!empty($_REQUEST['writer']))      ?  strtolower ( filter_var( $_REQUEST['writer']     , FILTER_SANITIZE_STRING     )):  NULL ; //require 3
                        $dataBook [] =  ''; //for decription 4
                        $dataBook [] =  (!empty($_REQUEST['available']))   ?  strtolower ( filter_var( $_REQUEST['available']  , FILTER_SANITIZE_NUMBER_INT )):  NULL ; //require 5
                        $dataBook [] =  (!empty($_REQUEST['id']))          ?  strtolower ( filter_var( $_REQUEST['id']         , FILTER_SANITIZE_NUMBER_INT )):  NULL ;
                        $coverBook   =  $_FILES['coverBook'];
                        $description = (!empty($_REQUEST['description']) && !empty($_REQUEST['description'])) ? 
                        str_replace($order , ' ' , trim (strtolower ( filter_var( $_REQUEST['description'], FILTER_SANITIZE_STRING )), "\t\n\r\0\x0B\xC2\xA0") ) :  null ;
                        $dataBook [4] = $description;
                        
                        //call the function
                        if ($this->model_admin->addNewBook($dataBook,$coverBook,$err)) {
                            $msg = '  <div class="alert alert-success alert-dismissible fade show alert-add-book" role="alert"> 
                                        <p class="lead">
                                            <strong>the book is added with successfully</strong>
                                            you will redirect in 3s at your Dashboard
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </p>
                                    </div>
                                ';
                            $this->model_admin->redirect($msg,3,'index.php?req=add_book');
                        } else {
                            echo'  <div class="alert alert-danger alert-dismissible fade show alert-add-book" role="alert">
                                        <p class="lead"> ';
                                            foreach ($err as $errors) {echo $errors;}
                                            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                        </p>
                                    </div> ';
                            require "views/admin/add_book.php";
                                }
                    }else{  $this->model_admin->redirect('',0,'index.php?req=dashboard'); } //redirect 
                break;
                case 'add_user':
                    require 'views/admin/add_user.php';
                break;
                case 'new_user' :
                    if(isset($_POST['add_new_user'])){
                        //prepare the data
                        $dataUser [] =  (!empty($_REQUEST['fullname']))    ?  strtolower ( filter_var( $_REQUEST['fullname']   , FILTER_SANITIZE_STRING     )):  NULL ; //require 0
                        $dataUser [] =  (!empty($_REQUEST['username']))    ?  strtolower (  filter_var( $_REQUEST['username']     , FILTER_SANITIZE_EMAIL    )):  NULL ; //require 2 
                        $dataUser [] =  (!empty($_REQUEST['password']))      ?  strtolower ( filter_var( $_REQUEST['password']     , FILTER_SANITIZE_STRING     )):  NULL ; //require 3
                        $dataUser [] =  (!empty($_REQUEST['confirm-password']))   ?  strtolower ( filter_var( $_REQUEST['confirm-password']  , FILTER_SANITIZE_STRING )):  NULL ; //require 5
                        $dataUser [] =  random_int(0,1000);//id
                        $dataUser [] = (!empty($_REQUEST['privileges'])) ? $_REQUEST['privileges'] : 0 ; //user or admin privileges

                        //call the function
                        if ($this->model_admin->addNewUser($dataUser, $err)) {
                            $msg = '  <div class="alert alert-success alert-dismissible fade show alert-add-book" role="alert"> 
                                            <p class="lead">
                                                <strong> user is added with successfully</strong>
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </p> 
                                        </div> 
                                    ';
                            $this->model_admin->redirect($msg,3,'index.php?req=add_user');
                        } else {
                            if (is_array($err)) {
                                echo'  <div class="alert alert-danger alert-dismissible fade show alert-add-book" role="alert">
                                            <p class="lead"> ';
                                                foreach ($err as $errors) {echo $errors ; } 
                                                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                            </p>
                                        </div> ';
                            }
                            require "views/admin/add_user.php";
                            }
                    }else{  $this->model_admin->redirect('',0,'index.php?req=dashboard'); } //redirect 
                break;
                case 'edit_book':
                    require "views/admin/edit.book.php";
                break;
                case 'editBookAdmin':
                    $book = (isset ($_REQUEST['book']) ) ? $_REQUEST['book']:'';
                    $id = (isset ($_REQUEST['id']) ) ? $_REQUEST['id']:'';
                    //check request
                    if (isset($book) &&  $book != '' && isset($id) && is_numeric($id) && $id > 0 && $id[0] != 0 ) {
                        require "views/admin/editBookForm.php";
                    }else{  $this->model_admin->redirect('',0,'index.php?req=dashboard'); } //redirect 
                break;
                case 'update_book':
                if(isset($_POST['update_book'])){
                    $order   = array("\r\n", "\n", "\r");
                    //here i should cll the function form model
                    //prepare the data
                    $dataBook [] =  (!empty($_REQUEST['bookName']))    ?  strtolower ( filter_var( $_REQUEST['bookName']   , FILTER_SANITIZE_STRING     )):  NULL ; //require 0
                    $dataBook [] = '';//for cover book 
                    $dataBook [] =  (!empty($_REQUEST['lang']))        ?  strtolower ( filter_var( implode('-', $_REQUEST['lang'])       , FILTER_SANITIZE_STRING    )):  NULL ; //require 2 
                    $dataBook [] =  (!empty($_REQUEST['writer']))      ?  strtolower ( filter_var( $_REQUEST['writer']     , FILTER_SANITIZE_STRING     )):  NULL ; //require 3
                    $dataBook [] =  ''; //for description 4
                    $dataBook [] =  (!empty($_REQUEST['available']))   ?  strtolower ( filter_var( $_REQUEST['available']  , FILTER_SANITIZE_NUMBER_INT )):  NULL ; //require 5
                    $dataBook [] =  (!empty($_REQUEST['id']))          ?  strtolower ( filter_var( $_REQUEST['id']         , FILTER_SANITIZE_NUMBER_INT )):  NULL ;
                    $coverBook   =  $_FILES['coverBook'];
                    $current_cover = $_REQUEST['src_img'];
                    $description = (!empty($_REQUEST['description']) && !empty($_REQUEST['description'])) ? 
                    str_replace($order , ' ' , trim (strtolower ( filter_var( $_REQUEST['description'], FILTER_SANITIZE_STRING )), "\t\n\r\0\x0B\xC2\xA0") ) :  null ;
                    $dataBook [4] = $description;
                    //echo $dataBook[0];
                    //echo $this->model_admin->updateBook($dataBook,$coverBook,$err,$current_cover);
                    //call the function
                    if ($this->model_admin->updateBook($dataBook,$coverBook,$err,$current_cover)) {
                        $msg = '  <div class="alert alert-success alert-dismissible fade show alert-add-book" role="alert"> 
                                    <p class="lead">
                                        <strong>the book is updated with successfully</strong>
                                        you will redirect
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </p>
                                </div>
                            ';
                        $this->model_admin->redirect($msg,3,'index.php?req=edit_book');
                    } else {
                        echo'  <div class="alert alert-danger alert-dismissible fade show alert-add-book" role="alert">
                                    <p class="lead"> ';
                                        foreach ($err as $errors) {echo $errors;}
                                        echo '<p class = "lead"> you will redirect after reading this message </p>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                    </p>
                                </div> ';
                                // echo $_SERVER['HTTP_REFERER'];
                        header( "REFRESH:5; URL=".$_SERVER['HTTP_REFERER']);
                            }
                }else{  $this->model_admin->redirect('',0,'index.php?req=dashboard'); } //redirect 
                break;
                case 'del-book':
                    if(isset($_POST['name'])):
                        $bookName = $_POST['name'];
                        if($this->BOOKS[$bookName]['src_img'] != './views/images/no-book-cover.png'){
                            unlink($this->BOOKS[$bookName]['src_img']);//delete phpot
                        }
                            $this->model_admin->deleteBook($bookName);
                    endif;
                break;
                case 'del-user':
                    if(isset($_POST['userId'])):
                        $userId = $_POST['userId'];
                        unlink('./models/data/users/'.$this->DATA[$userId][1].'/books.csv');//remove file
                        rmdir('../models/data/users/'.$this->DATA[$userId][1]);//remove directory
                            $this->model_admin->deleteUser($userId);
                    endif;
                break;
                case 'edit_user':
                    require "views/admin/edit.user.php";
                break;
                case 'update_user_admin':
                    $id = (isset ($_REQUEST['id']) ) ? $_REQUEST['id']:'';
                    //check request
                    if ( isset($id) && is_numeric($id)) {
                        require "views/admin/edit.userForm.php";
                    }else{  $this->model_admin->redirect('',0,'index.php?req=dashboard'); } //redirect 
                break;
                case 'update_user':
                $id = (isset ($_REQUEST['id-arr']) ) ? $_REQUEST['id-arr']:'';
                    //check request
                    if(isset($_POST['update_user'])){
                        $data [] = (! empty( $_POST['fullname'] ) ) ? strtolower( filter_var(  $_POST['fullname'], FILTER_SANITIZE_STRING ) ) : null ; 
                        $data [] = (! empty( $_POST['email'] ) ) ? strtolower( filter_var( $_POST['email'] , FILTER_SANITIZE_EMAIL )  )    : null ;
                        $data [] = (! empty( $_POST['password'] ) ) ?  filter_var( $_POST['password'], FILTER_SANITIZE_STRING )    : null ;
                        $data [] = (! empty( $_POST['id'] ) )     ?  filter_var( $_POST['id'], FILTER_SANITIZE_STRING )          : null ;
                        $data [] = ( $_POST['privileges'] == 1 )   ?  filter_var( $_POST['privileges'], FILTER_SANITIZE_NUMBER_INT )   : 0 ; 
                        
                        rename('./models/data/users/'.$this->DATA[$id][1] , './models/data/users/'.$data[1]);
                        $this->DATA[$id][0] = $data[0];
                        $this->DATA[$id][1] = $data[1];
                        if($data[2] != null) { $this->DATA[$id][2] = password_hash($data[2],PASSWORD_BCRYPT ); }//hashing password
                        $this->DATA[$id][3] = $data[3];
                        $this->DATA[$id][4] = $data[4];
                        $this->model_admin->updateUser($this->DATA);
                        $msg = '  <div class="alert alert-success alert-dismissible fade show alert-add-book" role="alert"> 
                                    <p class="lead">
                                        <strong>user is updated with successfully</strong>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </p>
                                </div>
                            ';
                        $this->model_admin->redirect($msg,3,'index.php?req=edit_user');
                    }else{  $this->model_admin->redirect('',0,'index.php?req=dashboard'); } //redirect 
                break;
                case 'logout':
                    session_unset();
                    session_destroy();
                    $this->model_admin->redirect('',0,'index.php');
                exit;
                break;
                default:
                    //check if the book name dose not exist
                    require "views/admin/dashboard.php";
                break;
            endswitch;
        else:
            //if not exists the request i will include dashboard 
            require "views/admin/dashboard.php";
        endif;
    }
}

